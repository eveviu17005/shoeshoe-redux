import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return <ItemShoe data={item} index={index} />;
    });
  };
  render() {
    return (
      <div>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeShopReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
