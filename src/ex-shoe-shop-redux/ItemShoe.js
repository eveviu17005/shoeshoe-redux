import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, VIEW_DETAIL } from "./redux/constant/constant";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-text">{price}$</h2>
          </div>
          <button
            className="btn btn-secondary"
            onClick={() => {
              this.props.handleChangeDetail(this.props.index);
            }}
          >
            Xem chi tiet
          </button>
          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.data);
            }}
            className="btn btn-danger"
          >
            Chon mua
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    handleChangeDetail: (indexShoe) => {
      dispatch({
        type: VIEW_DETAIL,
        payload: {
          index: indexShoe,
        },
      });
    },
    handleAddToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
